# pslaughter

Hi!

![high five](https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExNWxyZnl6MzI1YWs3bW91NnFhZml2YzE5dml3MjlhZTU1cXgwdTJkcyZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/fm4WhPMzu9hRK/giphy.gif)

## My schedule has changed!

For the foreseeable future, this is my schedule:

<details>

I spend the work week with 80% focus time and 20% flex time.

- **Focus time** is where I am free from distractions and can easily take meetings, solve hard problems, and get in The-Groove :tm:.
- **Flex time** is where I will be sporadically online for short periods. During this time I can contirbute to discussions asynchronously, take care of small code reviews, and make small contributions.

This is distributed across the week as:

- 90+% of the week's focus time is on Tuesday, Thursday, Friday and Saturday. I'm available for most of the day these days.
- Monday and Wednesday I am mostly unavailable. I might have some **Flex time** on these days depending on what's going on.

</details>

## On Code Review

Here's my favorite way to efficiently review MR’s

<details>

### Shifting to review mode

1. Close all tabs. Close editors. Close distractions.
2. Open my list of MR’s to review. Open a clean terminal with the gdk running on the latest `gitlab` master branch. Open my editor.

### For each review

1. Start a timer (personally, I shoot for either 15m or 10m). The whole review might take longer. This just helps keep track of how much time has passed. Anticipate how many "time blocks" it should take to finish the review, and try to wrap things up during the last block.
2. Read context
3. Review code locally. Instead of changing branches, I like to apply the MR diff and stage changes to do this.  
   
   ```
   curl URL_TO_MR.diff | git apply && git add –-all
   ```
   
4. High level read-through. Code smells or maintainability issues? Duplicate code? Low cohesion? Modules or functions getting too big? Is the code fragile? Potential user-facing issues?
5. Review test coverage. Are there blocks of untested code (mutation testing FTW!)? Do we need to test at additional levels?
6. Line-for-line read-through. Undesirable low-level patterns? Polishing opportunities? Is the code fragile? Potential user-facing issues?
7. Manual exploratory testing. Were there any potential user-facing issues I can confirm based on the previous read-throughs?
8. If necessary, convert notes to actionable comments.
9. Finish!

</details>
